<?php

namespace Models;

class DemoModel extends BaseModel {
    public function getDate() {

        // $this->db->query("..."); - DBAL

        return date('Y-m-d H:i:s');
    }
}
