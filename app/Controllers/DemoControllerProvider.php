<?php

namespace Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;

class DemoControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function (Application $app) {
            $model = new \Models\DemoModel($app);

            $twigData = [ 'date' => $model->getDate() ];

            return $app['twig']->render('demo.twig', $twigData);
        });

        return $controllers;
    }
}
