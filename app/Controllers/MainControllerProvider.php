<?php

namespace Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;

class MainControllerProvider implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function (Application $app) {
            return $app['twig']->render('main.twig');
        });

        return $controllers;
    }
}
