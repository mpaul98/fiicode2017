<?php

$app->mount('/', new Controllers\IndexControllerProvider());
$app->mount('/app', new Controllers\MainControllerProvider());
$app->mount('/demo', new Controllers\DemoControllerProvider());
